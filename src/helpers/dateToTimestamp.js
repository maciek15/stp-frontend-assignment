import dayjs from 'dayjs'
import 'dayjs/locale/nn'
import customParseFormat from 'dayjs/plugin/customParseFormat'

dayjs.extend(customParseFormat)

export function dateToTimestamp (date, format, locale = 'en') {
  return dayjs(date, format, locale).valueOf()
}
