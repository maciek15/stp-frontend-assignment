export function sortArticles (articles, isSortingAsc) {
  switch (isSortingAsc) {
    case true:
      return articles.sort((a, b) => {
        return a.timestamp - b.timestamp
      })
    case false:
      return articles.sort((a, b) => {
        return b.timestamp - a.timestamp
      })
    default:
      return articles.sort((a, b) => {
        return b.timestamp - a.timestamp
      })
  }
}
