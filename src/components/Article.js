import React from 'react'
import PropTypes from 'prop-types'

function setPlaceholder (event, text) {
  event.target.src = `http://placehold.jp/b1b1b1/7e7e7e/280x280.png?text=${text}`
}

const Article = props => {
  const { image, title, date, preamble } = props

  return (
    <article className='stp-article'>
      <div className='stp-article__image'>
        <img
          src={image}
          alt={title}
          onError={event => {
            setPlaceholder(event, title)
          }}
        />
      </div>
      <div className='stp-article__content'>
        <header className='stp-article__header'>
          <h2 className='stp-article__header-title'>{title}</h2>
          <div className='stp-article__header-date'>{date}</div>
        </header>
        <div className='stp-article__preamble'>{preamble}</div>
      </div>
    </article>
  )
}

Article.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  date: PropTypes.string,
  preamble: PropTypes.string,
}

export default Article
