import React from 'react'
import PropTypes from 'prop-types'

import { DEFAULT_FILTERS } from 'config'
const STP_FILTER_ID_PREFIX = 'stp-filters'

const Filters = props => {
  const { onChange = () => {}, value = [] } = props
  return (
    <section className='stp-filters'>
      <header className='stp-filters__header'>Data sources</header>
      {DEFAULT_FILTERS.map(filter => (
        <div className='stp-filters__filter' key={filter}>
          <input
            type='checkbox'
            id={`${STP_FILTER_ID_PREFIX}__${filter}`}
            onChange={e => onChange(filter, e.target.checked)}
            checked={value.indexOf(filter) > -1}
          />
          <label htmlFor={`${STP_FILTER_ID_PREFIX}__${filter}`}>{filter}</label>
        </div>
      ))}
    </section>
  )
}

Filters.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.arrayOf(PropTypes.string),
}

export default Filters
