import { get } from './methods'
import { dateToTimestamp } from 'helpers'
import { DEFAULT_FILTERS } from 'config'

/**
 * Parse response body and check if error occured
 *
 * @param {Promise<any>} res
 * @returns {Promise<any>}
 */
async function handleResponse (res) {
  const response = await res.json()
  if (!res.ok) {
    return Promise.reject(new Error('Could not load articles.'))
  }
  return response
}

/**
 * Add timestamp to all articles to enable sorting by date
 *
 * @param {object} response
 * @returns {(T | {timestamp: *})[]}
 */
function parseArticlesResponse (response) {
  const articles = response.articles.map(item => {
    const timestamp = dateToTimestamp(item.date, 'D MMMM YYYY', 'nn')
    return { ...item, timestamp }
  })

  return articles
}

/**
 * Get articles from single endpoint
 *
 * @param {string} topicUrl
 * @returns {Promise<Response>}
 */
function getTopicArticles (topicUrl) {
  return get(topicUrl)
    .then(handleResponse)
    .then(parseArticlesResponse)
}

/**
 * Get filtered articles from API
 *
 * @param {array} types
 * @returns {Promise<*[]>}
 */
export async function getArticles (types) {
  // Load all articles if either empty array or wrong/empty argument is passed
  if (!Array.isArray(types) || types.length < 1) {
    types = DEFAULT_FILTERS
  }
  const topics = types.map(x => `/articles/${x}`)
  return Promise.all(topics.map(getTopicArticles)).then(data => {
    return [].concat.apply([], data)
  })
}
