import React from 'react'

import Article from 'components/Article'
import DateSort from 'components/DateSort'
import Filters from 'components/Filters'

import { getArticles } from 'api'
import { sortArticles } from 'helpers'

class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: true,
      error: null,
      articles: [],
      isSortingAsc: false,
      filters: [],
    }
    this.handleDateSort = this.handleDateSort.bind(this)
    this.handleFiltersChange = this.handleFiltersChange.bind(this)
    this.loadArticles = this.loadArticles.bind(this)
  }

  componentDidMount () {
    this.loadArticles()
  }

  loadArticles (filters) {
    const newState = {}
    if (filters) {
      newState.filters = filters
    }
    getArticles(filters)
      .then(data => {
        this.setState({ isLoading: false, articles: data, error: null, ...newState })
      })
      .catch(err => {
        this.setState({ isLoading: false, articles: [], error: err, ...newState })
      })
  }

  handleDateSort () {
    const { isSortingAsc } = this.state
    this.setState({ isSortingAsc: !isSortingAsc })
  }

  handleFiltersChange (filter, checked) {
    const newFilters = this.state.filters.filter(x => x !== filter)

    if (checked) {
      newFilters.push(filter)
    }
    this.loadArticles(newFilters)
  }

  getContent () {
    const { articles, error, isSortingAsc } = this.state
    const sortedArticles = sortArticles(articles, isSortingAsc)

    return error ? (
      <div className='stp-app__error'>
        Could not load articles. Click <span onClick={() => this.loadArticles()}>here</span> to try
        again...
      </div>
    ) : (
      <>
        {sortedArticles.length > 0 &&
          sortedArticles.map(article => <Article key={article.id} {...article} />)}
      </>
    )
  }

  render () {
    const { isSortingAsc, filters } = this.state
    const content = this.getContent()

    return (
      <div className='stp-app'>
        <Filters onChange={this.handleFiltersChange} value={filters} />
        <DateSort isSortingAsc={isSortingAsc} onClick={this.handleDateSort} />
        <div className='stp-app__articles'>{content}</div>
      </div>
    )
  }
}

export default App
