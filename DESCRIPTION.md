## What I have done

I have created an app that allows displaying a set of articles from backend service. As the server did not allow filtering by
query but only two separate endpoints, I create API call which enables either gathering all data from server or filtering
articles that belong to a given topic.

The other part of the app was to enable filtering by date. I managed to do this using `day.js` library as all publish
dates from articles were store in a string representing Norwegian format of date. Dates are parsed to UNIX timestamp which
then is used to sort articles.

I also added a placeholder for articles which do not have an image URL.

## Ways to improve
1. Filters and data sorting could be stored and stay the same when the user refreshes the page.
2. On mobile devices, a title could be truncated.
3. API could be written and documented in YAML. It would enable autogeneration of an API calls code.
4. `getArticles` could be improved to check if given filters can be used. If not, those should not be stored in APP and
    it could throw an error.
